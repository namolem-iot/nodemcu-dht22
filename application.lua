
--################
--# END settings #
--################

temperature = 0
humidity = 0

STATUS_OK = 1
STATUS_ERROR_CHECKSUM = 2
STATUS_ERROR_TIMEOUT = 3

-- DHT22 sensor logic
function getSensorData()
    dht=require("dht")
    status,temp,humi,temp_decimial,humi_decimial = dht.read(2)
    if( status == dht.OK ) then
        -- Prevent "0.-2 deg C" or "-2.-6"
        temperature = temp.."."..(math.abs(temp_decimial)/100)
        humidity = humi.."."..(math.abs(humi_decimial)/100)
        -- If temp is zero and temp_decimal is negative, then add "-" to the temperature string
        if(temp == 0 and temp_decimial<0) then
            temperature = "-"..temperature
        end

        return STATUS_OK,temperature, humidity
    elseif( status == dht.ERROR_CHECKSUM ) then
        print( "DHT Checksum error" )
        return STATUS_ERROR_CHECKSUM, 0,0
    elseif( status == dht.ERROR_TIMEOUT ) then
        return STATUS_ERROR_TIMEOUT, 0,0
    end
    -- Release module
    dht=nil
    package.loaded["dht"]=nil
end

temperatureDeviceId = "8b43699c-b53b-4af3-92c6-7dc2127bb18e"
humidityDeviceId = "cfc40d1c-c184-4cb6-9341-d51fbdea0e15"

function sendToSensors(temperature, humidity)
  con = nil
  con = net.createConnection(net.TCP, 0)

  con:on("receive", function(con, payloadout)
      if (string.find(payloadout, "201") ~= nil) then
          print("sensors-api: 201 Created");
      end
  end)

  con:on("connection", function(con, payloadout)
      print("Temperature: "..temp.." deg C")
      print("Humidity: "..hum.."%")
      print("Sending data to sensors-api...")

      payload = "[ { \"device_id\" : \"".. temperatureDeviceId.."\", \"value\" : "..temperature.."}, "..
      "  { \"device_id\" : \"".. humidityDeviceId.. "\", \"value\" : "..humidity.."} ]"
      -- Post data to Thingspeak
      requestText = "POST /api/v1/measurements HTTP/1.1\r\n"..
      "Host: 192.168.11.9:3007\r\n" ..
      "Connection: close\r\n" ..
      "Content-Type: application/json\r\n"..
      "Content-Length: " .. string.len(payload).."\r\n\r\n"..
      payload

      con:send(requestText)
  end)

  -- Connect to Thingspeak
  print("Connecting to 'https://192.168.11.9:3007/api/v1/measurements'")
  con:connect(3007,'192.168.11.9')
end

function sendToThingSpeak(temperature, humidity)
    con = nil
    con = net.createConnection(net.TCP, 0)

    con:on("receive", function(con, payloadout)
        if (string.find(payloadout, "Status: 200 OK") ~= nil) then
            print("Posted 200 OK to ThingSpeak");
        end
    end)

    con:on("connection", function(con, payloadout)
        print("Temperature: "..temp.." deg C")
        print("Humidity: "..hum.."%")
        print("Sending data to thingspeak...")
        -- Post data to Thingspeak
        con:send(
            "POST /update?api_key=" .. cfg.ThingSpeakApiKey ..
            "&field1=" .. temp ..
            "&field2=" .. hum ..
            " HTTP/1.1\r\n" ..
            "Host: api.thingspeak.com\r\n" ..
            "Connection: close\r\n" ..
            "Accept: */*\r\n" ..
            "User-Agent: Mozilla/4.0 (compatible; esp8266 Lua; Windows NT 5.1)\r\n" ..
            "\r\n")
    end)

    -- Connect to Thingspeak
    print("Connecting to 'http://api.thingspeak.com/update'")
    con:connect(80,'api.thingspeak.com')
end


status, temp, hum = getSensorData()

if status == STATUS_ERROR_TIMEOUT then
    print("DHT22 timeout")
    goToSleep()
elseif status == STATUS_ERROR_CHECKSUM then
    print("DHT22 error checksum")
    goToSleep()
elseif status == STATUS_OK then
    sendToSensors(temp, hum)
    sendToThingSpeak(temp, hum)
end

-- Watchdog loop, will force deep sleep the operation somehow takes to long
tmr.alarm(1,15000,1,function()
  print("15 sec timeout. Going to deep sleep")
  goToSleep()
 end)
