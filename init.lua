-- Thingspeak connect script with deep sleep
-- Remember to connect GPIO16 and RST to enable deep sleep

--############
--# Settings #
--############
cfg = {}
dofile("settings.lua")
--############

function startup()
  if file.open("init.lua") == nil then
    print("init.lua deleted or renamed")
  else
    print("Running")
    file.close("init.lua")
    dofile("application.lua")
  end
end

wifi_connect_event = function(T)
  print("Connection to AP(" .. T.SSID..") established!")
  print("Waiting for IP address..")
  if disconnect_ct ~= nil then disconnect_ct = nul end
end

wifi_got_ip_event = function(T)
  print("Wifi connection is ready! IP address is :".. T.IP)
  print("Startup will resume momentarily, you have 3 seconds to abort.")
  print("Waiting...")
  tmr.create():alarm(3000, tmr.ALARM_SINGLE, startup)
end

wifi_disconnect_event = function(T)
  if T.reason == wifi_eventmon.reason.ASSOC_LEAVE then
    --the station has disassociated from a previously connected AP
    return
  end
  local total_tries = 75
  print("\nWiFi connection to AP(" .. T.SSID .. ") has failed!")

  for key,val in pairs(wifi.eventmon.reason) do
    if val == T.reason then
      print("Disconnect reason: ".. val.."("..key..")")
      break
    end
  end

  if disconnect_ct == nil then
    disconnect_ct = 1
  else
    disconnect_ct = disconnect_ct+1
  end
  if disconnect_ct < total_tries then
    print("Retrying connection... (attempt "..(disconnect_ct+1).." of "..total_tries..")")
  else
    wifi.sta.disconnect()
    print("Aborting connection to AP!")
    disconnect_ct = nil
  end
end

--- INTERVAL ---
-- In milliseconds. Remember that the sensor reading,
-- reboot and wifi reconnect takes a few seconds
time_between_sensor_readings = 60000

goneToSleep = 0
function goToSleep()
  if (goneToSleep == 1) then return 1 end
  goneToSleep = 1

  print("Going to deep sleep for "..(time_between_sensor_readings/1000).." seconds")
  wifi.eventmon.unregister(wifi.eventmon.STA_CONNECTED)
  wifi.eventmon.unregister(wifi.eventmon.STA_GOT_IP)
  wifi.eventmon.unregister(wifi.eventmon.STA_DISCONNECTED)
  node.dsleep(time_between_sensor_readings*1000)
end

-- Register WiFi Station event callbacks
wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, wifi_connect_event)
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)
wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)

wifi.setmode(wifi.STATION)
wifi.sta.config({ssid=cfg.WifiSSID, pwd=cfg.WifiPassword, save=true})
